## Subsurface Test: 1D Drainage

This is an 1D drainage problem, where the soil column is initially fully saturated, with an open boundary in the bottom. Check Forsyth etal for more details (https://doi.org/10.1016/0309-1708(95)00020-J)